package com.intermedia.qa.steps;

import cucumber.api.java.en.When;

/**
 *
 */
public class SampleSteps {

    @When("user opens a browser")
    public void userOpensABrowser() {
        //Nothing to do - the browser is open in Setup.before()
    }
}

Intermedia Cucumber Selenium Test 
===========================  


# The Task  

A demo site exists at: https://www.saucedemo.com/

* username: standard_user
* username: performance_glitch_user
* password: secret_sauce

NB: there is a checkout information page which requires some details in 3 fields.   Enter any text you like in these fields to proceed.


## 1
Write a test that visits this site as user standard_user, and from the products page, adds all items to the cart, and proceeds to the checkout, and verifies that the total value of items is $140.34
## 2
Write another test that goes to the site as user performance_glitch_user, and adds all items, but this time, remove all items of type T-Shirt from the shopping cart, and continue to the checkout. Verify that the total value of items is $105.80
## 3
Write another test that goes to the site as user performance_glitch_user, and adds all items, but this time, remove all items of type Backpack from the shopping cart, and continue to the checkout. Verify that the total value of items is $107.95



   

# Setup      
 
1. Make sure you have Java 11 installed
    https://adoptium.net/?variant=openjdk11

2. Ensure you have a java IDE with the relevant plugins installed for cucumber. (E.g. Intellij IDEA)     
    https://www.jetbrains.com/idea/download  (Free Community Edition should be enough)

3. Run inside Intellij IDEA    
    Import as standard Maven project.    
    Then, set 'Cucumber java' defaults of this project as:    
    - Main class: `cucumber.api.cli.Main`       
    - Glue: `com.intermedia.qa`      
    - feature folder path: _point to folder where feature files are_  
    
 4. Chrome
 Ensure you have the latest version of Chrome installed
